import os
import environ
import requests

from django.test import Client
from django.test import TestCase
from django.urls import resolve

from tugas2_riwayat.views import *

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# class Tugas2UnitTest(TestCase):
#     def test_tugas2_riwayat_url_is_exist(self):
#         response = Client().get('/mahasiswa/riwayat/')
#         self.assertEqual(response.status_code, 200)

#     def test_tugas2riwayat_using_index_func(self):
#         found = resolve('/mahasiswa/riwayat/')
#         self.assertEqual(found.func, index)

#     # test for nilai api
#     def test_get_riwayar_api1(self):
#         response = requests.get('https://private-e52a5-ppw2017.apiary-mock.com/riwayat')
#         self.assertEqual(response.json(), get_riwayat())

 