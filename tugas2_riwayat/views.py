# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
import requests

from login_page.views import *
from .api_riwayat import get_riwayat

response = {}

def index(request):
	if not 'user_login' in request.session.keys():
		return HttpResponseRedirect(reverse('tugas2-mahasiswa'))
	else:
		set_data_for_session(response, request)
		response['riwayats'] = get_riwayat()
		html = 'tugas2_riwayat/riwayat.html'
		#response['rahasia'] = False
		return render(request, html, response)
#>>>>>>> 891d76f7107bb311c6a2ac42dfda383d680dac89
