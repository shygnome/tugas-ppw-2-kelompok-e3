from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo

from login_page.views import *
# Create your views here.
response = {}
def index(request):
    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('tugas2-mahasiswa'))
    else:
        set_data_for_session(response, request)
        todo = Todo.objects.all()
        response['todo'] = todo
        html = 'status.html'
        response['todo_form'] = Todo_Form
        return render(request, html, response)

def update_status_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['fill'] = request.POST['fill']
        todo = Todo(fill=response['fill'])
        todo.save()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')
		
def remove_todo(request):
    try:
        idObjek = request.POST['flag']
        Todo.objects.filter(id=idObjek).delete()
    except ValueError or KeyError:
        pass
    return HttpResponseRedirect('/update-status/')