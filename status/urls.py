from django.conf.urls import url
from .views import index, update_status_todo, remove_todo
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^update_status_todo', update_status_todo, name='update_status_todo'),
	url(r'^remove_todo', remove_todo, name='remove_todo'),
]
