from django.conf.urls import url
from .views import index, edit_profile,save_to_database

urlpatterns = [
	url(r'^$', index, name='index'),
        url(r'^edit/', edit_profile, name='edit'),
        url(r'^save/', save_to_database, name='save'),
]
