# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
import requests

from status.models import Todo
from login_page.models import Pengguna
from login_page.views import *
from .api_csui import *

response = {}

def index(request):
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect('/mahasiswa/')
    else:
        set_data_for_session(response, request)
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            pengguna = create_new_user(request)

        status = Todo.objects.count()
        lastStatus = Todo.objects.last()
        html = 'tugas2_profile/profile.html'
        return render(request, html, response)
	
def edit_profile(request):
    html = 'edit/update_profile.html'
    return render(request, html, response)

def save_to_database(request):
    if request.method == 'POST':
        first_name  = request.POST['firstName']
        last_name   = request.POST['lastName']
        image_url   = request.POST['imageUrl']
        email      = request.POST['email']
        profile_url = request.POST['profileUrl']

        request.user.profile.update({
            'first_name': first_name,
            'last_name': last_name,
            'image_url': image_url,
            'email': email,
            'profile_url': profile_url,
        })
        return JsonResponse({'save':'success'})
