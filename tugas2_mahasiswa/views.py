from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

# Create your views here.
def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('tugas2-mahasiswa:tugas2-profile:index'))
    else:
        return HttpResponseRedirect(reverse('login_page:login_page'))