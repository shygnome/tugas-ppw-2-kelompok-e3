from django.conf.urls import url, include
from .views import *

import tugas2_riwayat.urls as tugas2_riwayat
import tugas2_profile.urls as tugas2_profile
import tugas2_teman.urls as tugas2_teman
import status.urls as tugas2_status

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^status/', include(tugas2_status,namespace='tugas2-status')),
	url(r'^profile/', include(tugas2_profile,namespace='tugas2-profile')),
	url(r'^riwayat/', include(tugas2_riwayat,namespace='tugas2-riwayat')),
	url(r'^teman/', include(tugas2_teman,namespace='tugas2-teman')),
]