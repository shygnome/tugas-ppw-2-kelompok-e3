from __future__ import unicode_literals

from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class Pengguna(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True)
    nama = models.CharField('Nama', max_length=200)
    profile_pic = models.URLField()
    flag_nilai = models.BooleanField(default=False)
    email = models.EmailField(default="Kosong")
    linkedin_profile = models.URLField(default="Kosong")
    expertise = models.ManyToManyField('Expertise', default="Kosong")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

# Create your models here.
class Expertise(models.Model):
	expertise = models.CharField(max_length=20)

	def __str__(self):
		return str(self.expertise)