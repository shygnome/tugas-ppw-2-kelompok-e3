from django.shortcuts import render

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

from .models import Pengguna

response = {}

def index(request):
    html = 'layout/dummy_base.html'
    return render(request, html, response)
		
def login_page(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        html = 'session/profile.html'
        return render(request, html, response)
    else:
        response['connected'] = False
        html = 'login.html'
        return render(request, html, response)
		
def passing_login(request):
    print ("#==> masuk langsung")
    response['connected'] = False
    html = 'session/profile.html'
    return render(request, html, response)

### SESSION : GET and SET
def get_data_session(request):
    if get_data_user(request, 'user_login'):
        response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(res, request):
    res['connected'] = True
    res['author'] = request.session['user_login']
    res['access_token'] = request.session['access_token']
    res['kode_identitas'] = request.session['kode_identitas']
    res['role'] = request.session['role']


def profile(request):
    print ("#==> profile")
    ## sol : bagaimana cara mencegah error, jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
        html = 'session/profile.html'
        pengguna = create_new_user(request)
        return render(request, html, response)
    else:
        set_data_for_session(request)
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            pengguna = create_new_user(request)
        ## end of sol
        html = 'session/profile.html'
        return render(request, html, response)

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna