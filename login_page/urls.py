from django.conf.urls import url
from .views import index, profile, login_page, passing_login
from .custom_auth import auth_login, auth_logout

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login_mahasiswa/$', login_page, name='login_page'),
    url(r'^passing_login/$', passing_login, name='passing_login'),
    url(r'^profile/$', profile, name='profile'),
	
	# custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
	
]