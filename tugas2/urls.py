"""tugas2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url , include
from django.contrib import admin
from django.views.generic import RedirectView
import login_page.urls as login_page
import tugas2_mahasiswa.urls as tugas2_mahasiswa
import tugas2_riwayat.urls as tugas2_riwayat
import tugas2_profile.urls as tugas2_profile
import tugas2_teman.urls as tugas2_teman

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='home/', permanent=True), name='index'),
    url(r'^home/', include(login_page,namespace='login_page')),
    url(r'^mahasiswa/', include(tugas2_mahasiswa,namespace='tugas2-mahasiswa')),
    url(r'^tugas2-riwayat/', include(tugas2_riwayat, namespace='tugas2-riwayat')),
    url(r'^tugas2-profile/', include(tugas2_profile, namespace='tugas2-profile')),
    #url(r'^profile/edit/', include(tugas2_profile, namespace='profile/edit')),

]
