// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~:(id,firstName,headline,lastName,picture-url,public-profile-url,email-address)").result(onSuccess).error(onError);
}

// Handle the successful return from the API call
function onSuccess(data) {
    console.log(data);
    var obj = JSON.stringify(data)
    sessionStorage.setItem("data", obj);

    $('.profile-picture').html(
      '<img class="cover" src="'+ data.pictureUrl + '"/>'
    );
    $('.content').html(
      '<h3> Data Pribadi </h3>'+
      '<p> Nama   :' + data.firstName + ' ' + data.lastName + '</p>'+
      '<p> Status :' + data.headline + '</p>'+
      '<p> Email  :' + data.emailAddress + '</p>'+
      '<p> Profile LinkedIn : <a href="'+ data.publicProfileUrl +'">'+ data.publicProfileUrl +'</a></p>'
    )
}
// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

function logOut(){
  IN.User.logout(sendGoodByeMessage);
}

function sendGoodByeMessage(){
  console.log("Good Bye!");
}

function saveLinkedIn(){
  console.log("success");
  var data = sessionStorage.getItem("data");
  var obj = JSON.parse(data);
  insertDatabase(obj.firstName,obj.lastName,obj.pictureUrl,obj.emailAddress,obj.publicProfileUrl);
}

var insertDatabase = function(firstName,lastName,imageUrl,email,profileUrl) {
        $.ajax({
            method: "POST",
            //url: 'http://localhost:8000/mahasiswa/profile/save/',
            data: {
              firstName: firstName,
              lastName: lastName,
              imageUrl: imageUrl,
              email: email,
              profileUrl: profileUrl
            },
            success : function (user) {
                console.log(user);
            },
            error : function (error) {
                console.log(error);
            }
        });
    };

function saveExpertise(){

}
