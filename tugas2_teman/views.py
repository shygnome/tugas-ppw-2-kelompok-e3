from django.shortcuts import render
from login_page.models import Pengguna
from login_page.views import *

import json

response = {}

# Create your views here.
def index(request):
	if not 'user_login' in request.session.keys():
		return HttpResponseRedirect(reverse('tugas2-mahasiswa'))
	else:
		set_data_for_session(response, request)
		response['data'] = get_data_pengguna()
		html = 'profile/list_teman.html'
		return render(request, html, response)

def get_data_pengguna():
	query = Pengguna.objects.all()
	print(query)

	data = []
	for pengguna in query:
		temp = {}
		temp['username'] = pengguna.nama
		temp['npm'] = pengguna.kode_identitas
		temp['email'] = pengguna.email
		temp['expertise'] = []
		keahlian = pengguna.expertise.all()
		for item in keahlian:
			temp['expertise'].append(item)
		data.append(temp)
	return json.dumps(data)